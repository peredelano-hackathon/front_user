FROM node:18 as builder

WORKDIR /app

COPY package.json /app

RUN yarn install

COPY . /app

RUN yarn build

FROM nginx:1.21.1-alpine

COPY --from=builder /app/dist/spa /usr/share/nginx/html

EXPOSE 80
